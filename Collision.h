//
//  Collision.h
//  pacman_xcode
//
//  Created by Dmitry Basavin on 23.05.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman_xcode__Collision__
#define __pacman_xcode__Collision__

#include "dataTypes.h"

int CheckCollision(HitBox first, HitBox second);

#endif /* defined(__pacman_xcode__Collision__) */
