#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>

#include "helpers.h"
#include "GameObjects/main.h"
#include "Collision.h"
#include <time.h>

#include <vector>
using namespace std;

//Размеры игрового окна
const int SCREEN_WIDTH = 640; /* Ширина окна */
const int SCREEN_HEIGHT = 480; /* Высота окна */

//Ссылка на объект игрового окна
SDL_Window* gWindow = NULL;

//Ссылка на рендер
SDL_Renderer* gRenderer = NULL;

TTF_Font* font;

std::vector<VisibleObject*> visibleObjects;

void printToScreen(char *text, int x, int y){
	SDL_Color color = {0xFF, 0xFF, 0xFF, 0x00};
	  SDL_Surface* textSurface = TTF_RenderText_Solid( font, text, color);
	  SDL_Texture* textTexture = SDL_CreateTextureFromSurface( gRenderer, textSurface );
	  SDL_FreeSurface(textSurface);
	  SDL_Rect position={x,y,28,14};
	  SDL_RenderCopy( gRenderer, textTexture, NULL, &position );
	  SDL_DestroyTexture( textTexture );
}

int main( int argc, char* args[] )
{
	int gamescore = 0;
  srand((unsigned int) time(NULL));

  //Инициализируем SDL, создаем рендер и игровое окно
  if( !init(&gWindow, &gRenderer, SCREEN_WIDTH, SCREEN_HEIGHT) ) {
    printf( "Failed to initialize!\n" );
    exit(1);
  }
  
  font = TTF_OpenFont("lazy.ttf", 14);
  if(!font) {
    printf("TTF_OpenFont: %s\n", TTF_GetError());
  }

  //Создаем пакмана и привидения
  Pacman* pacman = new Pacman(gRenderer, {100,100});
  Ghost* blinky = new Ghost(gRenderer, {320, 240}, "sprites/ghosts/blinky.png");
  Ghost* inky = new Ghost(gRenderer, {320, 240}, "sprites/ghosts/inky.png");
  Ghost* pinky = new Ghost(gRenderer, {320, 240}, "sprites/ghosts/inky.png");
  Ghost* clyde = new Ghost(gRenderer, {320, 240}, "sprites/ghosts/clyde.png");

  // Заносим пакмана и привидения в общий массив
  visibleObjects.push_back(pacman);
  visibleObjects.push_back(blinky);
  visibleObjects.push_back(inky);
  visibleObjects.push_back(pinky);
  visibleObjects.push_back(clyde);

  // Создаем монетки
  for (int i=0;i<10;i++){
    visibleObjects.push_back(new Coin(gRenderer, {rand() % SCREEN_WIDTH, rand() % SCREEN_HEIGHT}));
  }

  // Создаем супер монетки
  for (int i=0;i<4;i++){
   	visibleObjects.push_back(new SuperCoin(gRenderer, {rand() % SCREEN_WIDTH, rand() % SCREEN_HEIGHT}));
  }

  // Создаем вишенку
  visibleObjects.push_back(new Cherry(gRenderer, {rand() % SCREEN_WIDTH, rand() % SCREEN_HEIGHT}));

  //Флаг выхода из основного цикла программы
  bool quit = false;

  //Переменная события
  SDL_Event e;

  //Текущий кадр анимации
  int frame = 0;

  //Продолжать цикл пока флаг quit не равен true
  while( !quit ) {

    //Обрабатываем все события в стеке событий
    while( SDL_PollEvent( &e ) != 0 ){

      //Пользователь завершает приложение
      if( e.type == SDL_QUIT ) {
        quit = true;
      }

    }

    //Select surfaces based on key press
    switch( e.key.keysym.sym ){
      case SDLK_UP:
        pacman->move(MOVE_UP);
        break;
      case SDLK_DOWN:
        pacman->move(MOVE_DOWN);
        break;
      case SDLK_LEFT:
        pacman->move(MOVE_LEFT);
        break;
      case SDLK_RIGHT:
        pacman->move(MOVE_RIGHT);
        break;
    }

    inky->move();
    pinky->move();
    blinky->move();
    clyde->move();

    //Очистка окна
    SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0x00 );
    SDL_RenderClear( gRenderer );

    for (int i = 0; i<visibleObjects.size(); i++) {
      if (i > 0 &&
          pacman != NULL &&
          CheckCollision(pacman->GetHitBox(), visibleObjects[i]->GetHitBox())) {
        if (visibleObjects[i]->clName == "Coin") {
          cout << "deleted Coin" << endl;
          delete visibleObjects[i];
          visibleObjects.erase(visibleObjects.begin() + i);
		  gamescore = gamescore+1;
        } else if (visibleObjects[i]->clName == "SuperCoin") {
          cout << "deleted SuperCoin" << endl;
          delete visibleObjects[i];
          visibleObjects.erase(visibleObjects.begin() + i);
        } else if (visibleObjects[i]->clName == "Ghost") {
          cout << "Ooouch!";
          quit = true;
        }
      }

      visibleObjects[i] -> render(frame);
    }
	//char str[1];
	//sprintf(str[0], "%d", gamescore);

	//char str[2];
	//str[0] = gamescore + '48';
	//str[1] = 0;

//	char symbol = gamescore + '48';
	printToScreen("123", 0,0);
	//(sprintf("%d", gamescore));
    SDL_RenderPresent( gRenderer );
    //Переходим к следующему кадру
    frame++;
    SDL_Delay(50);
  }

  TTF_CloseFont(font);
  //Очищаем память
  SDL_DestroyRenderer( gRenderer );
  SDL_DestroyWindow( gWindow );;

  //Выходим из подсистем SDL
  IMG_Quit();
  SDL_Quit();

  return 0;
}
