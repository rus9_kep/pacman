//
//  helpers.cpp
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#include "helpers.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <stdio.h>

bool init(SDL_Window** gWindow, /* Окно для отрисовки */
          SDL_Renderer** gRenderer, /* Объект, занимающийся отрисовкой (Рендер) */
          const int SCREEN_WIDTH, /* Высота окна */
          const int SCREEN_HEIGHT) {/* Ширина окна */

  //Флаг успешности инициализации
  bool success = true;
  
  //Инициализируем SDL
  if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
    printf( "Ошибка при инициализации SDL! Ошибка SDL: %s\n", SDL_GetError() );
    success = false;
  } else {
    //Устанавливаем линейную фильтрацию текстур
    if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) ) {
      printf( "Внимание: Линейная фильтрация текстур не включена!" );
    }
    
    //Create window
    gWindow[0] = SDL_CreateWindow( "Pacman", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
    if( gWindow == NULL ) {
      printf( "Невозможно создать окно! Ошибка SDL: %s\n", SDL_GetError() );
      success = false;
    } else {
      //Создаем рендер с вертикальной синхронизацией для окна
      gRenderer[0] = SDL_CreateRenderer( gWindow[0], -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
      if( gRenderer == NULL ) {
        printf( "Рендер не может быть создан! Ошибка SDL: %s\n", SDL_GetError() );
        success = false;
      } else {
        //Устанавливаем основной цвет рендера
        SDL_SetRenderDrawColor( gRenderer[0], 0xFF, 0xFF, 0xFF, 0xFF );
        
        //Инициализируем загрузку PNG
        int imgFlags = IMG_INIT_PNG;
        if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
          printf( "Ошибка при инициализации SDL_image! Ошибка SDL_image: %s\n", IMG_GetError() );
          success = false;
        }
        
        if( TTF_Init() == -1 ) {
		  printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
		  success = false;
		}
      }
    }
  }
  
  return success;
}
