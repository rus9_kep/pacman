﻿#ifndef __GAME_OBJECT_H__
#define __GAME_OBJECT_H__ 1
#include <iostream>
//размеры игрового поля
#define MAP_WIDTH 500  //ширина игрового поля
#define MAP_HEIGHT 500  //высота игрового поля
using namespace std;

//перечисление направлений движений
enum MOVE_DIRECTIONS{
	MD_LEFT = 0,  //влево
	MD_RIGTH = 2,   //вправо
	MD_UP = 1,  //вверх
	MD_DOWN = 3,    //вниз
};

//координаты объекта на игровом поле
struct Coords {
	int x; //координата х
	int y; //координата y
};

//Класс объектов
class GameObject {
public:
	Coords position;
	GameObject(){
		position.x=0; //установка координаты x=0
		position.y=0; //установка координаты y=0
	}
	// метод для установки координат
	int setPosition(int x, int y){
		position.x = x; //позиция х
		position.y = y;  //позиция y
	}
};

//Секция интерфейсов
//интерфейс движения
class IMovable {
public:
	int virtual move(MOVE_DIRECTIONS direction) = 0; //виртуальный метод для движения
};

//интерфейс отрисовки
class IRenderable {
public:
	int virtual render() = 0;  //виртуальный метод отрисовки
};
//Классы объектов
// Класс для видимых объектов
class VisibleObject: public GameObject, public IRenderable {
public:
    int	render() { cout<<"Объект отрисовывается"<<endl; }  //отрисовка объекта
};

// Класс общий
class Character: public VisibleObject, public IMovable {
public:

	//определение направления движения и изменение координат
    int	move(MOVE_DIRECTIONS direction);
	int	render() { cout<<"Персонаж отрисовывается"<<endl; }//отрисовка объекта
};

// Класс Монетки
class Coin: public VisibleObject {
public:
	//инициализация монетки
	Coin () { cout <<"монетка инициализирована"<< endl; }
	//отрисовка монетки
    int	render() { cout<<"Монетка отрисовывается с координатами ("<< position.x << ";" <<position.y<< ")" <<endl; }
};
// Класс Вишенки
class Cherry: public VisibleObject {
public:
	//инициализация вишенки
	Cherry(){ cout <<"вишенка инициализирована"<< endl; }
	//отрисовка вишенки
    int	render() { cout<<"Вишенка отрисовывается"<<endl; }
};
// Класс Супер монетки
class SuperCoin: public VisibleObject {
public:
	//инициализация супер монетки
	SuperCoin() { cout <<"супер-монетка инициализирована"<< endl; }
	//отрисовка супер монетки
    int	render() { cout<<"С. монетка отрисовывается"<<endl; }
};
// Класс Пакмэна
class Pacman: public Character {
public:
	//инициализация пакмена
	Pacman(){ cout <<"пакмен инициализирован"<< endl; }
	//отрисовка пакмена
	int	render() { cout<<"Пакмен отрисовывается"<<endl; }
};

//Класс Приведений
class Ghost: public Character {
public:
	//инициализация приведения
	Ghost(){ cout<<"Привидение инициализировано"<<endl; }
	//отрисовка приведения
	int	render() { cout<<"Приведение отрисовывается"<<endl; }
};
#endif
