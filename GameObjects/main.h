//
//  main.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman__GameObjects__
#define __pacman__GameObjects__

#include "VisibleObject.h"

#include "Pacman.h"
#include "Ghost.h"

#include "Coin.h"
#include "SuperCoin.h"
#include "Cherry.h"

#endif /* defined(__pacman__GameObjects__) */
