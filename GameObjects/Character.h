//
//  Character.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman__Character__
#define __pacman__Character__

#include <iostream>
#include <SDL.h>
#include "VisibleObject.h"
#include "Interfaces/IMovable.h"

// Класс персонажа (Пакмен, привидения)
class Character: public VisibleObject, public IMovable {
public:
  Character(SDL_Renderer* renderer, Coords pos, std::string image_path):VisibleObject(renderer, pos, image_path){};
  int move(MOVE_DIRECTIONS direction);
};

#endif /* defined(__pacman__Character__) */
