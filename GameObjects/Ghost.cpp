//
//  Ghost.cpp
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#include "Ghost.h"

Ghost::Ghost(SDL_Renderer* renderer, Coords pos, std::string image_path):Character(renderer, pos, image_path) {
  clName = "Ghost";
  
  position = pos;
  
  dimensions.width = 36;
  dimensions.height = 40;
  
  spriteClips = new SDL_Rect[2];
  
  framesCount = 2;
  
  spriteClips[ 0 ].x = 0;
  spriteClips[ 0 ].y = 0;
  spriteClips[ 0 ].w = 36;
  spriteClips[ 0 ].h = 40;
  
  spriteClips[ 1 ].x = 36;
  spriteClips[ 1 ].y = 0;
  spriteClips[ 1 ].w = 36;
  spriteClips[ 1 ].h = 40;
  
  moveDirection = static_cast<MOVE_DIRECTIONS>(rand() % 4);
}

int Ghost::move() {
  int p = rand() % 100;
  if (p < 5) {
    moveDirection = static_cast<MOVE_DIRECTIONS>(rand() % 4);
  }
  if (position.x < 0 ) {
    moveDirection = MOVE_RIGHT;
  }
  if (position.x + dimensions.width > 640) {
    moveDirection = MOVE_LEFT;
  }
  if (position.y < 0) {
    moveDirection = MOVE_DOWN;
  }
  if (position.y + dimensions.height > 480) {
    moveDirection = MOVE_UP;
  }
  Character::move(moveDirection);
  return 1;
}

Ghost::~Ghost(){
  delete[] spriteClips;
}
