//
//  Pacman.cpp
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#include "Pacman.h"

Pacman::Pacman(SDL_Renderer* renderer, Coords pos):Character(renderer, pos, "sprites/pacman.png"){
  clName = "Pacman";
  
  dimensions.width = 40;
  dimensions.height = 40;
  
  spriteClips = new SDL_Rect[2];
  
  framesCount = 2;
  
  spriteClips[ 0 ].x = 0;
  spriteClips[ 0 ].y = 0;
  spriteClips[ 0 ].w = 40;
  spriteClips[ 0 ].h = 40;
  
  spriteClips[ 1 ].x = 40;
  spriteClips[ 1 ].y = 0;
  spriteClips[ 1 ].w = 40;
  spriteClips[ 1 ].h = 40;
}

Pacman::~Pacman(){
  delete [] spriteClips;
}

int Pacman::move(MOVE_DIRECTIONS direction) {
  switch (direction) {
    case MOVE_UP:
      flipType = SDL_FLIP_VERTICAL;
      angle = 270;
      break;
      
    case MOVE_RIGHT:
      flipType = SDL_FLIP_NONE;
      angle = 0;
      break;
      
    case MOVE_DOWN:
      flipType = SDL_FLIP_NONE;
      angle = 90;
      break;
      
    case MOVE_LEFT:
      flipType = SDL_FLIP_VERTICAL;
      angle = 180;
      break;
  }
  
  Character::move(direction);
  return 1;
}

int Pacman::render(int frame) {
  VisibleObject::render(frame, angle, flipType);
  return 1;
}
