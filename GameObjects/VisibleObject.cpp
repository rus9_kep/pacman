//
//  VisibleObject.cpp
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#include "VisibleObject.h"

HitBox VisibleObject::GetHitBox(){
  HitBox hitBox;
  hitBox.x = position.x;
  hitBox.y = position.y;
  hitBox.w = dimensions.width;
  hitBox.h = dimensions.height;
  return hitBox;
}

int VisibleObject::render(int frame) {
  sprite.render(position.x, position.y, &(spriteClips[frame % framesCount]));
  return 1;
}

int VisibleObject::render(int frame, double angle, SDL_RendererFlip flipType){
  sprite.render(position.x, position.y, &(spriteClips[frame % framesCount]), angle, NULL, flipType);
  return 1;
}

VisibleObject::VisibleObject(SDL_Renderer* renderer, Coords pos, std::string image_path){
  position = pos;
  sprite.setRenderer(renderer);
  sprite.loadFromFile(image_path);
  
  framesCount = 1;
}

VisibleObject::~VisibleObject() {
  sprite.free();
}
