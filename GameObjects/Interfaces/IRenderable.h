//
//  IRenderable.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman_IRenderable__
#define __pacman_IRenderable__

// Интерфейс для метода отрисовки объектов (render)
class IRenderable {
public:
  int virtual render(int frame) = 0;
};

#endif /* defined(__pacman_IRenderable__) */
