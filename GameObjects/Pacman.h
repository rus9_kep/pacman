//
//  Pacman.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman__Pacman__
#define __pacman__Pacman__

#include "Character.h"
#include <SDL.h>

// Класс Пакмэна
class Pacman: public Character {
public:
  double angle;
  SDL_RendererFlip flipType;
  
  int move(MOVE_DIRECTIONS direction);
  int render (int frame);
  
  Pacman(SDL_Renderer* renderer, Coords pos);
  ~Pacman();
};
#endif /* defined(__pacman__Pacman__) */
