﻿//
//  Character.cpp
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#include "Character.h"

int Character::move(MOVE_DIRECTIONS direction) {
  switch (direction) {
    case MOVE_UP:
      position.y-=6;
      break;
      
    case MOVE_RIGHT:
      position.x+=6;
      break;
      
    case MOVE_DOWN:
      position.y+=6;
      break;
      
    case MOVE_LEFT:
      position.x-=6;
      break;
  }
  return 1;
}
