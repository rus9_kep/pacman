//
//  Coin.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman__Coin__
#define __pacman__Coin__

#include <SDL.h>
#include "../dataTypes.h"
#include "VisibleObject.h"

class Coin: public VisibleObject {
public:
  Coin(SDL_Renderer* renderer, Coords pos);
  ~Coin();
};

#endif /* defined(__pacman__Coin__) */
